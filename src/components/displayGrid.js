import React, { Component } from 'react';
import Griddle from 'griddle-react';

class DisplayGrid extends Component {
  constructor(props) {
    super(props);
    // This line is important!
    this.onRefreshClick = this.onRefreshClick.bind(this);
   }
    onRefreshClick()
   {
      console.log("RowClickcalled");
   };
  render() {
    return (
      <div className="well">				
  	   <Griddle results={this.props.load} showFilter={true} showSettings={true} columnMetadata={[{'Key':1,'Value':"JobRunId"},{'key':2,'Value': "JobStatus"}, {'Key':3,'Value':"UserLogin"}, {'Key':4,'Value':"BatchId"}, {'Key':5,'Value':"JobName"}]} resultsPerPage={5} enableInfiniteScroll={true} onRowClick={this.onRefreshClick} useFixedHeader={true} bodyHeight={400} />
 			</div>
    );
  }
}
DisplayGrid.propTypes={
  load: React.PropTypes.array.isRequired
};
export default DisplayGrid;
