import React, { Component } from 'react';
import { Link } from 'react-router'
class Header extends Component {
  constructor(props) {
    super(props);
    this.Add = this.Add.bind(this);
   }
   Add(a,b)
   {
      return a+b;
   };
  render() {
    console.log("inside Header component");
    console.log(this.Add(2,3));
    return (
        <nav className="navbar navbar-default">
          <div className="container-fluid">
          <div className="navbar-header">
            <a className="navbar-brand" href="/">{this.props.AppName}</a>
          </div>
              <ul className="nav nav-pills">
                <li role="presentation" className="active"><Link to="/">Home</Link></li>
                <li role="presentation"><Link to="/about">About</Link></li>
                <li role="presentation"><Link to="/refresh">Refresh</Link></li>
              </ul>
              {this.props.children}
          </div>
        </nav>
		);
  }
}
Header.propTypes={
  AppName: React.PropTypes.string.isRequired
};
export default Header;
