import React, { Component } from 'react';
class Refresh extends Component {
    constructor(props) {
    super(props);
    this.onRefresh = this.onRefresh.bind(this);
   }
    onRefresh() {
      this.props.onRefreshClick();
  };
  render() {
   return (
			<div>				
 <button type="button" className="btn btn-success" onClick={this.onRefresh}>Refresh</button>
			</div>
		);
  }
}
Refresh.propTypes={
  onRefreshClick: React.PropTypes.func
};
export default Refresh;