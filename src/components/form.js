import React, { Component } from 'react';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import FormControl from 'react-bootstrap/lib/FormControl';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import HelpBlock from 'react-bootstrap/lib/HelpBlock';
import Polygon from '../Models/JobStatus';
class NameForm extends Component {
  constructor(props) {
    super(props);
    var job = new Polygon();
    this.state = {JobStatusModel : job};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({JobStatusModel:{width: event.target.value} });
  }
  
  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.JobStatusModel.width);
    event.preventDefault();
  }

  render() {
      console.log("re rendering")
    return (
      <form onSubmit={this.handleSubmit}>
      <FormGroup
          controlId="formBasicText"
        >
          <ControlLabel>Enter the Job Details</ControlLabel>
          <FormControl
            type="text"
            value={this.state.JobStatusModel.width}
            placeholder="Enter JobName"
            onChange={this.handleChange}
          />
          <FormControl
            type="text"
            value={this.state.JobStatusModel.width}
            placeholder="Enter JobName"
            onChange={this.handleChange}
          />
          <FormControl.Feedback />
          <HelpBlock>Validation is based on string length.</HelpBlock>
        </FormGroup>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}
export default NameForm;