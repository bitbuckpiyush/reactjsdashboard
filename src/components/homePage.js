import React, { Component } from 'react';
import $ from 'jquery';
import DisplayGrid from './displayGrid';
import Refresh from './common/refresh';
import ButtonGroup from 'react-bootstrap/lib/ButtonGroup';
import Button from 'react-bootstrap/lib/Button';
import Modal from 'react-bootstrap/lib/Modal'
import Popover from 'react-bootstrap/lib/Popover';
import Tooltip from 'react-bootstrap/lib/Tooltip';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';
import Form from './form';
import 'whatwg-fetch';
class Home extends Component {
   constructor(props) {
    super(props);
    this.state = {jobs:[],count:0,value: 'fdfd', showModal: false}
    // This line is important!
    this.loadJobs = this.loadJobs.bind(this);
    this.onRefreshClick = this.onRefreshClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.close = this.close.bind(this);
    this.open = this.open.bind(this);
   }
   handleChange(event) {
    this.setState({value: event.target.value});
  }
    close() {
      console.log("close is here");
     
     this.setState({showModal: false });
     console.log(this.state.showModal);
  }

  open() {
    console.log("open is here")
    
    this.setState({ showModal: true });
    console.log(this.state.showModal);
  }
  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
  }
   loadJobs() {
     console.log("Load jobs called");
     this.serverRequest=$.get("http://10.150.226.69:8085/api/JobStatus", function (result) {
     this.setState({ jobs: result, count: result.length });  
    }.bind(this));
  
   };
   onRefreshClick()
   {
      this.loadJobs();
      console.log("Home Refresh Method called");
   };
  render() {
    const popover = (
      <Popover id="modal-popover" title="popover">
        very popover. such engagement
      </Popover>
    );
    const tooltip = (
      <Tooltip id="modal-tooltip">
        wow.
      </Tooltip>
    );
   console.log("render called");
    console.log(this.state.jobs.length);
		return (
        <div>
        
        <Modal show={this.state.showModal} onHide={this.close}>
          <Modal.Header closeButton>
            <Modal.Title>Modal heading</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form/>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close}>Close</Button>
          </Modal.Footer>
        </Modal>
        <DisplayGrid load={this.state.jobs}/>
        <ButtonGroup>
         <Button bsStyle="primary" bsSize="small" onClick={this.onRefreshClick}>Refresh</Button>
         <Button bsStyle="primary" bsSize="small" onClick={this.open}>Add</Button>
        </ButtonGroup>
        </div>
		);
  };
  
    componentWillMount()
    {
    console.log("componentWillMount Called");
    this.loadJobs();
    //this.setState({ jobs: datas, count: datas.length });  
    };
    componentDidMount()
    {
      
    console.log("componentDidMount called");
    };
    componentWillUpdate()
    {
    console.log("componentWillUpdate called");
    };
    componentDidUpdate()
    {
      console.log("componentDidUpdateCalled");
    };
    componentWillUnmount()
    {
      console.log("componentWillUnmountCalled");
    };
    // shouldComponentUpdate(nextProps, nextState) 
    // {
    // console.log("shouldcomponentupdate called");
    // //return nextState.jobs.length !== this.state.jobs.length;
    // };
}
Home.getDefaultProps = {
 AppName: "TestApp"
};

export default Home;