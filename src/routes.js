import { Router, Route, Link, browserHistory, DefaultRoute, IndexRoute } from 'react-router'
import React from 'react';
import App from './App'
import homePage from './components/homePage';
export default (
    <Route component={App} path="/">
      <IndexRoute component={homePage} />
    </Route>
);