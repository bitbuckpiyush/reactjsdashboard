import React, { Component } from 'react';
import Header from './components/common/header';
import {RouteHandler} from 'react-router';
class App extends Component {
  render() {
			return (
				<div>
					<Header AppName="DataPublisher Dashboard"/>
					{this.props.children}
				</div>
			);
  }
}

export default App;
