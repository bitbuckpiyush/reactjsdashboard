import React from 'react';
import ReactDOM from 'react-dom';
import Header from './components/common/header';

it('renders Header component without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Header AppName="Dashboard" />, div);
  var testObj = new Header();
  expect(testObj.Add(2,3)).toEqual(5);
});

