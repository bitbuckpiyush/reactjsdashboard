import React from 'react';
import ReactDOM from 'react-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/css/bootstrap-theme.min.css';
import {Router,browserHistory,hashHistory,Route,IndexRoute} from 'react-router';
import App from './App';
import About from './components/about/aboutPage';
import Home from './components/homePage';
import Refresh from './components/common/refresh';
import ModalWindow from './modal';
(function(win) {
	function render(){
	ReactDOM.render(
		<Router history={hashHistory}>
    <Route path="/" component={App}>
	  <IndexRoute component={Home}/>
    <Route path="/about" component={About}/>
	  <Route path="/refresh" component={Refresh}/>
		<Route path="/modal" component={ModalWindow}/>
    </Route>
  </Router>, document.getElementById('root'));		
	}
	render();
 })(window);
